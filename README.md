# Chirp-8

_Chirp-8_ is a [CHIP-8](https://en.wikipedia.org/wiki/CHIP-8) emulator.

CHIP-8 is a language as well as a virtual machine definition. The CHIP-8 language is meant to be interpreted on the CHIP-8 virtual machine. Chirp-8 emulates the CHIP-8 virtual machine on your actual host machine and interpret the CHIP-8 program in that context.

The main purpose of this project is to get more experience with the rust language and low level programming in general.

## How does it work?

    1. Load the program into memory, starting at address 0x200
    2. Start interpreting instruction at 0x200
    3. Step further through the instructions in a memory in a linear way, unless control flow instructions (jumps, subroutines) are encountered

## About CHIP-8
### Resources
    - https://en.wikipedia.org/wiki/Central_processing_unit
    - https://en.wikipedia.org/wiki/Instruction_cycle
    - https://en.wikipedia.org/wiki/CHIP-8
    - https://storage.googleapis.com/wzukusers/user-34724694/documents/5c83d6a5aec8eZ0cT194/CHIP-8%20Classic%20Manual%20Rev%201.3.pdf
    - http://mattmik.com/files/chip8/mastering/chip8.html
    - https://chip-8.github.io/

### Properties
    - It is not a system architecture. It is a programming language and the virtual machine the language runs on.
    - the virtual machine specifies
        - a set of 16 8-bit registers
        - 4096 8-bit memory locations
            - first 512 locations are reserved for the interpreter itself
            - starting at location 0x200 the actual program will start
        - subroutines with a callstack (on original hardware for up to 12 levels of nesting (48 bytes in the memory))
        - two timers
            - one delay timer which can be set to a value, read from and will automatically count downwards at 60Hz
            - one sound timer which also counts downwards at 60Hz and will play a sound when reaching zero
        - a 16 keys input keyboard
        - a 64*32 pixels monochrome display
        - a set of 35 opcodes which are 2 bytes in length and notated as four hexadecimal digits like `00E0` or `8F21`
    - A CHIP-8 program is loaded directly and fully into a specific memory segment (starting at 0x200)

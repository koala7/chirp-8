mod console_monitor;
mod controlunit;
mod interpreter;
mod loader;
mod machine;


fn main() {
    env_logger::init();
    println!("Welcome to Chirp-8!");
    interpreter::interpret(&loader::read_program("./data/BREAKOUT"));
}

use log::{debug};
use std::fs::read;

pub fn read_program(path: &str) -> Vec<u8> {
    println!("Reading program {}", path);
    let content: Vec<u8> = read(path).expect("Error reading program");
    assert_eq!(
        content.len() % 2,
        0,
        "Program was in unexpected format. CHIP-8 instructions are 16bit values."
    );
    debug!("Raw bytes of program:\n{:x?}", content);
    content
}

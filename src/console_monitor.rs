use crate::machine::{Machine, SCREEN_WIDTH, SCREEN_HEIGHT};


const PIXEL_ON_CHARACTER: &str = "□";
const PIXEL_OFF_CHARACTER: &str = "■";

pub fn draw(virtual_machine: &Machine) {
    for y in 0..SCREEN_HEIGHT {
        for x in 0..SCREEN_WIDTH {
            let pixel = if virtual_machine.screen_buffer[x][y] == true { PIXEL_ON_CHARACTER } else { PIXEL_OFF_CHARACTER };
            print!("{}", pixel);
        }
        println!();
    }
}

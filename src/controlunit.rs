use log::{debug, warn};
use twelve_bit::u12;
use twelve_bit::u12::*;
use rand::Rng;

use crate::machine::{Machine, SCREEN_HEIGHT, SCREEN_WIDTH};


trait DecimalIndexable {
    fn get_first_decimal_digit(&self) -> u8;
    fn get_second_decimal_digit(&self) -> u8;
    fn get_third_decimal_digit(&self) -> u8;
    fn get_digit(&self, index: usize) -> u8;
}

impl DecimalIndexable for u8 {
    fn get_first_decimal_digit(&self) -> u8 {
        self / 100
    }
    fn get_second_decimal_digit(&self) -> u8 {
        (self - (100 * self.get_first_decimal_digit())) / 10
    }
    fn get_third_decimal_digit(&self) -> u8 {
        self - (100 * self.get_first_decimal_digit()) - (10 * self.get_second_decimal_digit())
    }
    fn get_digit(&self, index: usize) -> u8 {
        let mask: u8 = 1 << index;
        let mask_applied: u8 = self & mask;
        mask_applied >> index
    }
}


#[allow(non_camel_case_types)]
pub enum Operation {
    op_00EE,
    op_1NNN,
    op_2NNN,
    op_3XNN,
    op_4XNN,
    op_6XNN,
    op_7XNN,
    op_8XY0,
    op_8XY2,
    op_8XY3,
    op_8XY4,
    op_8XY5,
    op_ANNN,
    op_CXNN,
    op_DXYN,
    op_EXA1,
    op_FX07,
    op_FX15,
    op_FX18,
    op_FX29,
    op_FX33,
    op_FX65,
}


#[allow(non_snake_case)]
pub fn op_00EE(virtual_machine: &mut Machine) {
    debug!("execute 00EE instruction");
    virtual_machine.program_counter = virtual_machine.pop_stack();
}

#[allow(non_snake_case)]
pub fn op_1NNN(virtual_machine: &mut Machine, jump_address: U12) {
    debug!("execute 1NNN instruction");

    virtual_machine.program_counter = jump_address;
}

#[allow(non_snake_case)]
pub fn op_2NNN(virtual_machine: &mut Machine, subroutine_address: U12) {
    debug!("execute 2NNN instruction");

    let resume_address: U12 = virtual_machine.program_counter;
    virtual_machine.push_to_stack(resume_address);
    virtual_machine.program_counter = subroutine_address;
}

#[allow(non_snake_case)]
pub fn op_3XNN(virtual_machine: &mut Machine, register: usize, compare_value: u8) {
    debug!("execute 3XNN instruction");

    let register_value: u8 = virtual_machine.get_register_value(register);
    if register_value == compare_value {
        debug!("Actually skipping the next execution");
        let next_execution_point: U12 = virtual_machine.program_counter.checked_add(u12![2]).expect("something weird");
        virtual_machine.program_counter = next_execution_point;
    }
    else {
        debug!("Do not skip the next execution");
    }
}

#[allow(non_snake_case)]
pub fn op_4XNN(virtual_machine: &mut Machine, register: usize, compare_value: u8) {
    debug!("execute 4XNN instruction");

    let register_value: u8 = virtual_machine.get_register_value(register);
    if register_value != compare_value {
        debug!("Actually skipping the next execution");
        let next_execution_point: U12 = virtual_machine.program_counter.checked_add(u12![2]).expect("something weird");
        virtual_machine.program_counter = next_execution_point;
    }
    else {
        debug!("Do not skip the next execution");
    }
}

#[allow(non_snake_case)]
pub fn op_6XNN(virtual_machine: &mut Machine, register: usize, value: u8) {
    debug!("execute 6XNN instruction");
    virtual_machine.set_register_value(register, value);
}

#[allow(non_snake_case)]
pub fn op_7XNN(virtual_machine: &mut Machine, register: usize, addend: u8) {
    debug!("execute 7XNN instruction");
    let previous: u8 = virtual_machine.get_register_value(register);
    virtual_machine.set_register_value(register, previous.wrapping_add(addend));
}

#[allow(non_snake_case)]
pub fn op_8XY0(virtual_machine: &mut Machine, register_x: usize, register_y: usize) {
    virtual_machine.set_register_value(
        register_x, 
        virtual_machine.get_register_value(register_y)
    );
}

#[allow(non_snake_case)]
pub fn op_8XY2(virtual_machine: &mut Machine, register_x: usize, register_y: usize) {
    let result = virtual_machine.get_register_value(register_x) & virtual_machine.get_register_value(register_y);
    virtual_machine.set_register_value(register_x, result);
}

#[allow(non_snake_case)]
pub fn op_8XY3(virtual_machine: &mut Machine, register_x: usize, register_y: usize) {
    let result = virtual_machine.get_register_value(register_x) ^ virtual_machine.get_register_value(register_y);
    virtual_machine.set_register_value(register_x, result);
}

#[allow(non_snake_case)]
pub fn op_8XY4(virtual_machine: &mut Machine, register_x: usize, register_y: usize) {
    let result: (u8, bool) = virtual_machine.get_register_value(register_x).overflowing_add(virtual_machine.get_register_value(register_y));
    virtual_machine.set_register_value(register_x, result.0);
    let vf: u8 = if result.1 { 0x1 } else { 0x0 };
    virtual_machine.set_register_value(0xF, vf);
}

#[allow(non_snake_case)]
pub fn op_8XY5(virtual_machine: &mut Machine, register_x: usize, register_y: usize) {
    let result: (u8, bool) = virtual_machine.get_register_value(register_x).overflowing_sub(virtual_machine.get_register_value(register_y));
    virtual_machine.set_register_value(register_x, result.0);
    let vf: u8 = if result.1 { 0x0 } else { 0x1 };
    virtual_machine.set_register_value(0xF, vf);
}

#[allow(non_snake_case)]
pub fn op_ANNN(virtual_machine: &mut Machine, address: U12) {
    debug!("execute ANNN instruction");
    virtual_machine.set_i_register_value(address);
}

#[allow(non_snake_case)]
pub fn op_CXNN(virtual_machine: &mut Machine, register: usize, mask: u8) {
    debug!("execute CXNN instruction");
    let random: u8 = rand::thread_rng().gen();
    virtual_machine.set_register_value(register, random & mask);
}

#[allow(non_snake_case)]
pub fn op_DXYN(virtual_machine: &mut Machine, x_register: usize, y_register: usize, bytes: u16) {
    let sprite_pointer: U12 = virtual_machine.get_i_register_value();
    let x_base_position = virtual_machine.get_register_value(x_register);
    let y_base_position = virtual_machine.get_register_value(y_register);

    let mut didASetPixelChangeToUnset = false;
    for row_index in 0..(bytes as u8) {
        let sprite_row: u8 = virtual_machine.get_memory(sprite_pointer.checked_add(U12::from(row_index)).unwrap());

        for column_index in 0..8 {
            let (x, y) = (
                (x_base_position + column_index) % SCREEN_WIDTH as u8, 
                (y_base_position + row_index) % SCREEN_HEIGHT as u8
            );
            let old_pixel_value = virtual_machine.get_pixel_value(x, y);
            let sprite_pixel_value = sprite_row.get_digit(7 - column_index as usize) != 0;
            let new_pixel_value = old_pixel_value ^ sprite_pixel_value;
            if old_pixel_value == true && new_pixel_value == false {
                didASetPixelChangeToUnset = true;
            }
            virtual_machine.draw_pixel_to_buffer(x, y, new_pixel_value);
        }
    }
    virtual_machine.set_register_value(0xF, if didASetPixelChangeToUnset { 0x01 } else { 0x00 });
}

#[allow(non_snake_case)]
pub fn op_EXA1(virtual_machine: &mut Machine, register: usize) {
    // EXA1 	Skip the following instruction if the key corresponding to the hex value currently stored in register VX is not pressed
    warn!("!! EXA1 not implemented yet !!");
}

#[allow(non_snake_case)]
pub fn op_FX07(virtual_machine: &mut Machine, register: usize) {
    debug!("execute FX07 instruction");
    virtual_machine.set_register_value(register, virtual_machine.delay_timer);
}

#[allow(non_snake_case)]
pub fn op_FX15(virtual_machine: &mut Machine, register: usize) {
    debug!("execute FX15 instruction");
    let value: u8 = virtual_machine.get_register_value(register);
    virtual_machine.delay_timer = value;
}

#[allow(non_snake_case)]
pub fn op_FX18(virtual_machine: &mut Machine, register: usize) {
    debug!("execute FX18 instruction");
    let value: u8 = virtual_machine.get_register_value(register);
    virtual_machine.sound_timer = value;
}

#[allow(non_snake_case)]
pub fn op_FX29(virtual_machine: &mut Machine, digit: u16) {
    debug!("execute FX29 instruction");
    if digit > 0xF { panic!["Should have been one hex digit"]; }
    let sprite_address: U12 = u12![Machine::FONT_OFFSET_ADDRESS + digit * 5];
    virtual_machine.set_i_register_value(sprite_address);
}

#[allow(non_snake_case)]
pub fn op_FX33(virtual_machine: &mut Machine, register: usize) {
    debug!("execute FX33 instruction");

    let register_value: u8 = virtual_machine.get_register_value(register);

    let first_digit: u8 = register_value.get_first_decimal_digit();
    let second_digit: u8 = register_value.get_second_decimal_digit();
    let third_digit: u8 = register_value.get_third_decimal_digit();

    let i_address: U12 = virtual_machine.get_i_register_value();
    let i_next_address: U12 = i_address.checked_add(u12![1]).expect("something weird");
    let i_next_next_address: U12 = i_address.checked_add(u12![2]).expect("something weird");

    virtual_machine.set_memory(i_address, first_digit);
    virtual_machine.set_memory(i_next_address, second_digit);
    virtual_machine.set_memory(i_next_next_address, third_digit);

    debug!("Set memory {:x?} to decimal value {}", i_address, first_digit);
    debug!("Set memory {:x?} to decimal value {}", i_next_address, second_digit);
    debug!("Set memory {:x?} to decimal value {}", i_next_next_address, third_digit);
}

#[allow(non_snake_case)]
pub fn op_FX65(virtual_machine: &mut Machine, max_register_index: usize) {
    debug!("execute FX65 instruction");

    for register_index in 0..max_register_index + 1 {
        let memory_address: U12 = virtual_machine.get_i_register_value().checked_add(u12![register_index as u16]).expect("something weird");
        virtual_machine.set_register_value(register_index, virtual_machine.get_memory(memory_address));
    }

    let memory_address: U12 = virtual_machine.get_i_register_value().checked_add(u12![max_register_index as u16 + 1]).expect("something weird");
    virtual_machine.set_i_register_value(memory_address);
}

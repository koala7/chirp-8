use log::{debug, info, warn};
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::render::Canvas;
use sdl2::video::Window;
use std::time::{SystemTime, Duration};
use std::thread;

use crate::machine::{Machine,SCREEN_HEIGHT,SCREEN_WIDTH};
use crate::controlunit::Operation;
use crate::controlunit;

use twelve_bit::u12;
use twelve_bit::u12::*;

trait Indexable {
    // TODO are the return types sensible?
    fn get_second_digit(&self) -> u16;
    fn get_third_digit(&self) -> u16;
    fn get_fourth_digit(&self) -> u16;
    fn get_third_and_fourth_digit(&self) -> u8;
    fn get_second_and_third_and_fourth_digit(&self) -> U12;
}

impl Indexable for u16 {
    fn get_second_digit(&self) -> u16 {
        (self << 4) >> 12
    }
    fn get_third_digit(&self) -> u16 {
        (self << 8) >> 12
    }
    fn get_fourth_digit(&self) -> u16 {
        (self << 12) >> 12
    }
    fn get_third_and_fourth_digit(&self) -> u8 {
        ((self << 8) >> 8) as u8
    }
    fn get_second_and_third_and_fourth_digit(&self) -> U12 {
        u12![(self << 4) >> 4]
    }
}


const CLOCK_SPEED_IN_HERTZ: u64 = 500;


pub fn interpret(program_bytecode: &Vec<u8>) -> () {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window(
            "Chirp-8 — A CHIP-8 interpreter",
            640,
            320,
        )
        .position_centered()
        .build()
        .unwrap();
    let mut canvas: Canvas<Window> = window
        .into_canvas()
        .target_texture()
        .present_vsync()
        .build()
        .unwrap();

    canvas.set_logical_size(64, 32).unwrap();

    let mut virtual_machine = Machine::create();
    virtual_machine.print_memory();
    virtual_machine.load_font();
    virtual_machine.copy_program_into_memory(&program_bytecode);
    virtual_machine.print_memory();

    let mut cycle_count : u128 = 0;
    println!("Start to interpret program…");
    loop {
        let time_before_cycle = SystemTime::now();
        {
            debug!("Cycle #{}", cycle_count);
            cpu_cycle(&mut virtual_machine);
            cycle_count += 1;

            virtual_machine.update_timers_if_needed();

            draw(&virtual_machine, &mut canvas);
        }
        let time_after_cycle = SystemTime::now();

        let cycle_duration: Duration = time_after_cycle.duration_since(time_before_cycle).unwrap();        
        let desired_cycle_duration: Duration = Duration::from_millis(1000 / CLOCK_SPEED_IN_HERTZ);

        let compensation_sleep_duration: Duration = match desired_cycle_duration.checked_sub(cycle_duration) {
            Some(x) => x,
            None => {
                warn!("Cycle took longer than the desired clock speed.");
                Duration::from_millis(0)
            }
        };

        debug!("Waiting for {:?}", compensation_sleep_duration);
        thread::sleep(compensation_sleep_duration);
    }
}

fn draw(virtual_machine: &Machine, canvas: &mut Canvas<Window>) {
    let before = SystemTime::now();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.set_draw_color(Color::RGB(0xFF, 0, 0xFF));
    for y in 0..SCREEN_HEIGHT {
        for x in 0..SCREEN_WIDTH {
            if virtual_machine.screen_buffer[x][y] == true {
                canvas.draw_point(Point::new(x as i32, y as i32)).unwrap();
            }
        }
    }
    let after = SystemTime::now();
    canvas.present();

    let after_present = SystemTime::now();
    debug!("Rendering took {:?} and presenting took {:?}", after.duration_since(before), after_present.duration_since(after));
}

fn cpu_cycle(virtual_machine: &mut Machine) {
    fetch_stage(virtual_machine);
    let instruction = decode_stage(&virtual_machine);
    execution_stage(virtual_machine, instruction);
}

fn fetch_stage(virtual_machine: &mut Machine) {
    let instruction_address = virtual_machine.program_counter;

    // Fetch instruction
    let first: u8 = virtual_machine.get_memory(instruction_address);
    let second: u8 = virtual_machine.get_memory(instruction_address.wrapping_add(u12![1]));
    let instruction = u16::from_be_bytes([first, second]);

    // Move instruction into instruction_register
    virtual_machine.instruction_register = instruction;
    
    // Point PC to subsequent instruction
    let next_execution_point: U12 = virtual_machine.program_counter.checked_add(u12![2]).expect("something weird");
    virtual_machine.program_counter = next_execution_point;
}

fn decode_stage(virtual_machine: &Machine) -> Operation {
    let instruction = virtual_machine.instruction_register;
    if instruction == 0x00EE {
        info!("{:x?} was 00EE operation — Return from a subroutine", instruction);
        return Operation::op_00EE;
    }
    else if instruction >> 12 == 0x1 {
        info!("{:x?} was 1NNN operation — Jump to address NNN", instruction);
        return Operation::op_1NNN;
    }
    else if instruction >> 12 == 0x2 {
        info!("{:x?} was 2NNN operation — Execute subroutine starting at address NNN", instruction);
        return Operation::op_2NNN;
    }
    else if instruction >> 12 == 0x3 {
        info!("{:x?} was 3XNN operation — Skip the following instruction if the value of register VX equals NN", instruction);
        return Operation::op_3XNN;
    }
    else if instruction >> 12 == 0x4 {
        info!("{:x?} was 4XNN operation — Skip the following instruction if the value of register VX is not equal to NN", instruction);
        return Operation::op_4XNN;
    }
    else if instruction >> 12 == 0x6 {
        info!("{:x?} was 6XNN operation — Store number NN in register VX", instruction);
        return Operation::op_6XNN;
    }
    else if instruction >> 12 == 0x7 {
        info!("{:x?} was 7XNN operation — Add the value NN to register VX", instruction);
        return Operation::op_7XNN;
    }
    else if (instruction >> 12 == 0x8) & ((instruction << 12) >> 12 == 0x0) {
        info!("{:x?} was 8XY0 operation — Store the value of register VY in register VX", instruction);
        return Operation::op_8XY0;
    }
    else if (instruction >> 12 == 0x8) & ((instruction << 12) >> 12 == 0x2) {
        info!("{:x?} was 8XY2 operation — Set VX to VX AND VY", instruction);
        return Operation::op_8XY2;
    }
    else if (instruction >> 12 == 0x8) & ((instruction << 12) >> 12 == 0x3) {
        info!("{:x?} was 8XY3 operation — Set VX to VX XOR VY", instruction);
        return Operation::op_8XY3;
    }
    else if (instruction >> 12 == 0x8) & ((instruction << 12) >> 12 == 0x4) {
        info!("{:x?} was 8XY4 operation — Add the value of register VY to register VX
        Set VF to 01 if a carry occurs
        Set VF to 00 if a carry does not occur", instruction);
        return Operation::op_8XY4;
    }
    else if (instruction >> 12 == 0x8) & ((instruction << 12) >> 12 == 0x5) {
        info!("{:x?} was 8XY5 operation — Subtract the value of register VY from register VX
        Set VF to 00 if a borrow occurs
        Set VF to 01 if a borrow does not occur", instruction);
        return Operation::op_8XY5;
    }
    else if instruction >> 12 == 0xA {
        info!("{:x?} was ANNN operation — Store memory address NNN in register I", instruction);
        return Operation::op_ANNN;
    }
    else if instruction >> 12 == 0xC {
        info!("{:x?} was CXNN operation — Set VX to a random number with a mask of NN", instruction);
        return Operation::op_CXNN;
    }
    else if instruction >> 12 == 0xD {
        info!("{:x?} was DXYN operation — Draw a sprite at position VX, VY with N bytes of sprite data starting at the address stored in I
        Drawing XORs with current screen buffer!
        Set VF to 01 if any set pixels are changed to unset, and 00 otherwise", instruction);
        return Operation::op_DXYN;
    }
    else if (instruction >> 12 == 0xE) & ((instruction << 8) >> 8 == 0xA1) {
        info!("{:x?} was EXA1 operation — Skip the following instruction if the key corresponding to the hex value currently stored in register VX is not pressed", instruction);
        return Operation::op_EXA1;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x07) {
        info!("{:x?} was FX07 operation — Store the current value of the delay timer in register VX", instruction);
        return Operation::op_FX07;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x15) {
        info!("{:x?} was FX15 operation — Set the delay timer to the value of register VX", instruction);
        return Operation::op_FX15;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x18) {
        info!("{:x?} was FX18 operation — Set the sound timer to the value of register VX", instruction);
        return Operation::op_FX18;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x29) {
        info!("{:x?} was FX29 operation — Set I to the memory address of the sprite data corresponding to the hexadecimal digit stored in register VX", instruction);
        return Operation::op_FX29;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x33) {
        info!("{:x?} was FX33 operation — Store the binary-coded decimal equivalent of the value stored in register VX at addresses I, I+1, and I+2", instruction);
        return Operation::op_FX33;
    }
    else if (instruction >> 12 == 0xF) & ((instruction << 8) >> 8 == 0x65) {
        info!("{:x?} was FX65 operation — Fill registers V0 to VX inclusive with the values stored in memory starting at address I
        I is set to I + X + 1 after operation", instruction);
        return Operation::op_FX65;
    }
    panic!["Unknown instruction {:x?} found.", instruction];
}

fn execution_stage(virtual_machine: &mut Machine, operation: Operation) {
    match operation {
        Operation::op_00EE => {
            controlunit::op_00EE(virtual_machine);
        }
        Operation::op_1NNN => {
            let nnn = virtual_machine.instruction_register.get_second_and_third_and_fourth_digit();
            controlunit::op_1NNN(virtual_machine, nnn);
        }
        Operation::op_2NNN => {
            let nnn = virtual_machine.instruction_register.get_second_and_third_and_fourth_digit();
            controlunit::op_2NNN(virtual_machine, nnn);
        }
        Operation::op_3XNN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let nn = virtual_machine.instruction_register.get_third_and_fourth_digit();
            controlunit::op_3XNN(virtual_machine, x, nn);
        }
        Operation::op_4XNN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let nn = virtual_machine.instruction_register.get_third_and_fourth_digit();
            controlunit::op_4XNN(virtual_machine, x, nn);
        }
        Operation::op_6XNN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let nn = virtual_machine.instruction_register.get_third_and_fourth_digit();
            controlunit::op_6XNN(virtual_machine, x, nn);
        }
        Operation::op_7XNN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let nn = virtual_machine.instruction_register.get_third_and_fourth_digit();
            controlunit::op_7XNN(virtual_machine, x, nn);
        }
        Operation::op_8XY0 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            controlunit::op_8XY0(virtual_machine, x, y);
        }
        Operation::op_8XY2 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            controlunit::op_8XY2(virtual_machine, x, y);
        }
        Operation::op_8XY3 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            controlunit::op_8XY3(virtual_machine, x, y);
        }
        Operation::op_8XY4 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            controlunit::op_8XY4(virtual_machine, x, y);
        }
        Operation::op_8XY5 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            controlunit::op_8XY5(virtual_machine, x, y);
        }
        Operation::op_ANNN => {
            let nnn = virtual_machine.instruction_register.get_second_and_third_and_fourth_digit();
            controlunit::op_ANNN(virtual_machine, nnn);
        }
        Operation::op_CXNN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let nn = virtual_machine.instruction_register.get_third_and_fourth_digit();
            controlunit::op_CXNN(virtual_machine, x, nn);
        }
        Operation::op_DXYN => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            let y = virtual_machine.instruction_register.get_third_digit().into();
            let n = virtual_machine.instruction_register.get_fourth_digit().into();
            controlunit::op_DXYN(virtual_machine, x, y, n);
        }
        Operation::op_EXA1 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_EXA1(virtual_machine, x);
        }
        Operation::op_FX07 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX07(virtual_machine, x);
        }
        Operation::op_FX15 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX15(virtual_machine, x);
        }
        Operation::op_FX18 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX18(virtual_machine, x);
        }
        Operation::op_FX29 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX29(virtual_machine, x);
        }
        Operation::op_FX33 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX33(virtual_machine, x);
        }
        Operation::op_FX65 => {
            let x = virtual_machine.instruction_register.get_second_digit().into();
            controlunit::op_FX65(virtual_machine, x);
        }
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::*;

    #[test]
    fn op_00EE() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x00);
        machine.set_memory(u12![0x201], 0xEE);
        machine.stack = vec![u12![0x12F], u12![0x32A]];

        cpu_cycle(&mut machine);
        
        assert_eq!(u12![0x32A], machine.program_counter);
        assert_eq!(vec![u12![0x12F]], machine.stack);
    }

    #[test]
    fn op_1NNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x1A);
        machine.set_memory(u12![1], 0xFB);
        machine.program_counter = u12![0];

        cpu_cycle(&mut machine);

        assert_eq!(u12![0xAFB], machine.program_counter);
    }

    #[test]
    fn op_2NNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x2A);
        machine.set_memory(u12![1], 0xFB);
        machine.program_counter = u12![0];

        cpu_cycle(&mut machine);

        assert_eq!(u12![0xAFB], machine.program_counter);
        assert_eq!(u12![2], machine.stack[0]);
    }

    #[test]
    fn op_3XNN_skip() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x3A);
        machine.set_memory(u12![1], 0x82);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0x82);

        cpu_cycle(&mut machine);

        assert_eq!(u12![4], machine.program_counter);
    }

    #[test]
    fn op_3XNN_no_skip() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x3A);
        machine.set_memory(u12![1], 0x82);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0x83);

        cpu_cycle(&mut machine);

        assert_eq!(u12![2], machine.program_counter);
    }

    #[test]
    fn op_4XNN_skip() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x4A);
        machine.set_memory(u12![1], 0x83);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0x82);

        cpu_cycle(&mut machine);

        assert_eq!(u12![4], machine.program_counter);
    }

    #[test]
    fn op_4XNN_no_skip() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x4A);
        machine.set_memory(u12![1], 0x82);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0x82);

        cpu_cycle(&mut machine);

        assert_eq!(u12![2], machine.program_counter);
    }

    #[test]
    fn op_6XNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x61);
        machine.set_memory(u12![1], 0x23);
        machine.program_counter = u12![0];
        
        cpu_cycle(&mut machine);

        assert_eq!(0x23, machine.get_register_value(1));
    }

    #[test]
    fn op_7XNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x7A);
        machine.set_memory(u12![1], 0x82);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0x22);

        cpu_cycle(&mut machine);

        assert_eq!(0xA4, machine.get_register_value(0xA));
    }

    #[test]
    fn op_7XNN_wraps() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0x7A);
        machine.set_memory(u12![1], 0x03);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 0xFE);

        cpu_cycle(&mut machine);

        assert_eq!(0x01, machine.get_register_value(0xA));
    }

    #[test]
    fn op_8XY0() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA0);
        machine.set_register_value(0xA, 0xFA);

        cpu_cycle(&mut machine);
        
        assert_eq!(0xFA, machine.get_register_value(0x2));
    }

    #[test]
    fn op_8XY2() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA2);
        machine.set_register_value(0x2, 0b10110010);
        machine.set_register_value(0xA, 0b11110000);

        cpu_cycle(&mut machine);
        
        assert_eq!(0b10110000, machine.get_register_value(0x2));
    }

    #[test]
    fn op_8XY3() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA3);
        machine.set_register_value(0x2, 0b10110010);
        machine.set_register_value(0xA, 0b11110000);

        cpu_cycle(&mut machine);
        
        assert_eq!(0b01000010, machine.get_register_value(0x2));
    }

    #[test]
    fn op_8XY4_no_overflow() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA4);
        machine.set_register_value(0x2, 0x0A);
        machine.set_register_value(0xA, 0x17);

        cpu_cycle(&mut machine);
        
        assert_eq!(0x21, machine.get_register_value(0x2));
        assert_eq!(0x0, machine.get_register_value(0xF));
    }

    #[test]
    fn op_8XY4_overflow() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA4);
        machine.set_register_value(0x2, 0xFE);
        machine.set_register_value(0xA, 0x03);

        cpu_cycle(&mut machine);
        
        assert_eq!(0x01, machine.get_register_value(0x2));
        assert_eq!(0x1, machine.get_register_value(0xF));
    }

    #[test]
    fn op_8XY5_no_borrow() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA5);
        machine.set_register_value(0x2, 0x75);
        machine.set_register_value(0xA, 0x13);

        cpu_cycle(&mut machine);
        
        assert_eq!(0x62, machine.get_register_value(0x2));
        assert_eq!(0x1, machine.get_register_value(0xF));
    }

    #[test]
    fn op_8XY5_borrow() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0x82);
        machine.set_memory(u12![0x201], 0xA5);
        machine.set_register_value(0x2, 0x14);
        machine.set_register_value(0xA, 0x23);

        cpu_cycle(&mut machine);
        
        assert_eq!(0xF1, machine.get_register_value(0x2));
        assert_eq!(0x0, machine.get_register_value(0xF));
    }

    #[test]
    fn op_ANNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0xA6);
        machine.set_memory(u12![1], 0xF2);
        machine.program_counter = u12![0];
        
        cpu_cycle(&mut machine);

        assert_eq!(u12![0x6F2], machine.get_i_register_value());
    }

    #[test]
    fn op_CXNN() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xCA);
        machine.set_memory(u12![0x201], 0b01001100);

        cpu_cycle(&mut machine);
        
        // Weak verification that bitmask was applied
        assert_eq!(0, machine.get_register_value(0xA) & 0b10110011);
    }

    #[test]
    fn op_DXYN_with_collision() {
        /* 
        Screen buffer before operation
        11100111
        01100111
        00011011

        Sprite to draw
        00111100
        11000011
        00111110

        Expected screen buffer after operation
        11011011
        10100100
        00100101
        */
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xDA);
        machine.set_memory(u12![0x201], 0x53);
        machine.set_register_value(0xA, 0x33);
        machine.set_register_value(0x5, 0x11);
        machine.set_i_register_value(u12![0x222]);
        machine.set_memory(u12![0x222], 0b00111100);
        machine.set_memory(u12![0x223], 0b11000011);
        machine.set_memory(u12![0x224], 0b00111110);

        machine.draw_pixel_to_buffer(0x33, 0x11, true);
        machine.draw_pixel_to_buffer(0x34, 0x11, true);
        machine.draw_pixel_to_buffer(0x35, 0x11, true);
        machine.draw_pixel_to_buffer(0x36, 0x11, false);
        machine.draw_pixel_to_buffer(0x37, 0x11, false);
        machine.draw_pixel_to_buffer(0x38, 0x11, true);
        machine.draw_pixel_to_buffer(0x39, 0x11, true);
        machine.draw_pixel_to_buffer(0x3A, 0x11, true);

        machine.draw_pixel_to_buffer(0x33, 0x12, false);
        machine.draw_pixel_to_buffer(0x34, 0x12, true);
        machine.draw_pixel_to_buffer(0x35, 0x12, true);
        machine.draw_pixel_to_buffer(0x36, 0x12, false);
        machine.draw_pixel_to_buffer(0x37, 0x12, false);
        machine.draw_pixel_to_buffer(0x38, 0x12, true);
        machine.draw_pixel_to_buffer(0x39, 0x12, true);
        machine.draw_pixel_to_buffer(0x3A, 0x12, true);

        machine.draw_pixel_to_buffer(0x33, 0x13, false);
        machine.draw_pixel_to_buffer(0x34, 0x13, false);
        machine.draw_pixel_to_buffer(0x35, 0x13, false);
        machine.draw_pixel_to_buffer(0x36, 0x13, true);
        machine.draw_pixel_to_buffer(0x37, 0x13, true);
        machine.draw_pixel_to_buffer(0x38, 0x13, false);
        machine.draw_pixel_to_buffer(0x39, 0x13, true);
        machine.draw_pixel_to_buffer(0x3A, 0x13, true);

        cpu_cycle(&mut machine);

        assert_eq!(true, machine.get_pixel_value(0x33, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x34, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x35, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x36, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x37, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x38, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x39, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x3A, 0x11));

        assert_eq!(true, machine.get_pixel_value(0x33, 0x12));
        assert_eq!(false, machine.get_pixel_value(0x34, 0x12));
        assert_eq!(true, machine.get_pixel_value(0x35, 0x12));
        assert_eq!(false, machine.get_pixel_value(0x36, 0x12));
        assert_eq!(false, machine.get_pixel_value(0x37, 0x12));
        assert_eq!(true, machine.get_pixel_value(0x38, 0x12));
        assert_eq!(false, machine.get_pixel_value(0x39, 0x12));
        assert_eq!(false, machine.get_pixel_value(0x3A, 0x12));

        assert_eq!(false, machine.get_pixel_value(0x33, 0x13));
        assert_eq!(false, machine.get_pixel_value(0x34, 0x13));
        assert_eq!(true, machine.get_pixel_value(0x35, 0x13));
        assert_eq!(false, machine.get_pixel_value(0x36, 0x13));
        assert_eq!(false, machine.get_pixel_value(0x37, 0x13));
        assert_eq!(true, machine.get_pixel_value(0x38, 0x13));
        assert_eq!(false, machine.get_pixel_value(0x39, 0x13));
        assert_eq!(true, machine.get_pixel_value(0x3A, 0x13));

        assert_eq!(0x01, machine.get_register_value(0xF));
    }

    #[test]
    fn op_DXYN_without_collision() {
        /* 
        Screen buffer before operation
        00110011

        Sprite to draw
        10000100

        Expected screen buffer after operation
        10110111
        */
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xDA);
        machine.set_memory(u12![0x201], 0x51);
        machine.set_register_value(0xA, 0x33);
        machine.set_register_value(0x5, 0x11);
        machine.set_i_register_value(u12![0x222]);
        machine.set_memory(u12![0x222], 0b10000100);

        machine.draw_pixel_to_buffer(0x33, 0x11, false);
        machine.draw_pixel_to_buffer(0x34, 0x11, false);
        machine.draw_pixel_to_buffer(0x35, 0x11, true);
        machine.draw_pixel_to_buffer(0x36, 0x11, true);
        machine.draw_pixel_to_buffer(0x37, 0x11, false);
        machine.draw_pixel_to_buffer(0x38, 0x11, false);
        machine.draw_pixel_to_buffer(0x39, 0x11, true);
        machine.draw_pixel_to_buffer(0x3A, 0x11, true);

        cpu_cycle(&mut machine);

        assert_eq!(true, machine.get_pixel_value(0x33, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x34, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x35, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x36, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x37, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x38, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x39, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x3A, 0x11));

        assert_eq!(0x0, machine.get_register_value(0xF));
    }

    #[test]
    fn op_DXYN_wrapping_around() {
        /* 
        Screen buffer before operation
        0010[…]1101

        Sprite to draw
        11111111

        Expected screen buffer after operation
        1101[…]0010
        */
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xDA);
        machine.set_memory(u12![0x201], 0x51);
        machine.set_register_value(0xA, 0x3C);
        machine.set_register_value(0x5, 0x11);
        machine.set_i_register_value(u12![0x222]);
        machine.set_memory(u12![0x222], 0b11111111);

        machine.draw_pixel_to_buffer(0x00, 0x11, false);
        machine.draw_pixel_to_buffer(0x01, 0x11, false);
        machine.draw_pixel_to_buffer(0x02, 0x11, true);
        machine.draw_pixel_to_buffer(0x03, 0x11, false);
        machine.draw_pixel_to_buffer(0x3C, 0x11, true);
        machine.draw_pixel_to_buffer(0x3D, 0x11, true);
        machine.draw_pixel_to_buffer(0x3E, 0x11, false);
        machine.draw_pixel_to_buffer(0x3F, 0x11, true);

        cpu_cycle(&mut machine);

        assert_eq!(true, machine.get_pixel_value(0x00, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x01, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x02, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x03, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x3C, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x3D, 0x11));
        assert_eq!(true, machine.get_pixel_value(0x3E, 0x11));
        assert_eq!(false, machine.get_pixel_value(0x3F, 0x11));

        assert_eq!(0x1, machine.get_register_value(0xF));
    }

    #[test]
    fn op_FX07() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xFA);
        machine.set_memory(u12![0x201], 0x07);
        machine.delay_timer = 0x6B;

        cpu_cycle(&mut machine);
        
        assert_eq!(0x6B, machine.get_register_value(0xA));
    }

    #[test]
    fn op_FX15() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xFA);
        machine.set_memory(u12![0x201], 0x15);
        machine.set_register_value(0xA, 0xF2);

        cpu_cycle(&mut machine);
        
        assert_eq!(0xF2, machine.delay_timer);
    }

    #[test]
    fn op_FX18() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xFA);
        machine.set_memory(u12![0x201], 0x18);
        machine.set_register_value(0xA, 0xF2);

        cpu_cycle(&mut machine);
        
        assert_eq!(0xF2, machine.sound_timer);
    }

    #[test]
    fn FX29() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0x200], 0xF2);
        machine.set_memory(u12![0x201], 0x29);

        cpu_cycle(&mut machine);
        
        assert_eq!(u12![10], machine.get_i_register_value());
    }

    #[test]
    fn FX33() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0xFA);
        machine.set_memory(u12![1], 0x33);
        machine.program_counter = u12![0];
        machine.set_register_value(0xA, 148);
        machine.set_i_register_value(u12![0x50]);

        cpu_cycle(&mut machine);

        assert_eq!(0x1, machine.get_memory(u12![0x50]));
        assert_eq!(0x4, machine.get_memory(u12![0x51]));
        assert_eq!(0x8, machine.get_memory(u12![0x52]));
    }

    #[test]
    fn FX65() {
        let mut machine = Machine::create();
        machine.set_memory(u12![0], 0xF4);
        machine.set_memory(u12![1], 0x65);
        machine.program_counter = u12![0];
        machine.set_i_register_value(u12![0x50]);
        machine.set_memory(u12![0x50], 0xA);
        machine.set_memory(u12![0x51], 0x7);
        machine.set_memory(u12![0x52], 0x3);
        machine.set_memory(u12![0x53], 0xF);
        machine.set_memory(u12![0x54], 0xB);

        cpu_cycle(&mut machine);

        
        assert_eq!(0xA, machine.get_register_value(0));
        assert_eq!(0x7, machine.get_register_value(1));
        assert_eq!(0x3, machine.get_register_value(2));
        assert_eq!(0xF, machine.get_register_value(3));
        assert_eq!(0xB, machine.get_register_value(4));
        assert_eq!(u12![0x55], machine.get_i_register_value());
    }
}

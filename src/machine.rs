use log::debug;
use twelve_bit::u12::FailableInto;
use twelve_bit::u12::U12;
use twelve_bit::u12;
use std::time::{SystemTime, Duration};


const MEMORY_SIZE: usize = 4096;
pub const SCREEN_HEIGHT: usize = 32;
pub const SCREEN_WIDTH: usize = 64;

pub struct Machine {
    memory: [u8; MEMORY_SIZE],
    registers: [u8; 16],
    i_register: U12,
    pub stack: Vec<U12>,
    pub program_counter: U12,
    pub instruction_register: u16,
    pub delay_timer: u8,
    pub sound_timer: u8,
    timers_last_update_time: SystemTime,
    pub screen_buffer: [[bool; SCREEN_HEIGHT]; SCREEN_WIDTH],
}

impl Machine {

    pub const FONT_OFFSET_ADDRESS: u16 = 0x0;
    pub const PROGRAM_OFFSET_ADDRESS: u16 = 0x200;
    const TIMER_FREQUENCY_IN_HERTZ: u128 = 60;

    pub fn create() -> Machine {
        Machine {
            memory: [0x0; MEMORY_SIZE],
            registers: [0x0; 16],
            i_register: u12![0],
            stack: Vec::with_capacity(16),
            program_counter: u12![Machine::PROGRAM_OFFSET_ADDRESS],
            instruction_register: 0x0,
            delay_timer: 0x0,
            sound_timer: 0x0,
            timers_last_update_time: SystemTime::now(),
            screen_buffer: [[false; SCREEN_HEIGHT]; SCREEN_WIDTH],
        }
    }

    pub fn update_timers_if_needed(&mut self) {
        // Todo: Implement in a multithreaded way
        let now = SystemTime::now();
        let since_last_timer_update: Duration = now
            .duration_since(self.timers_last_update_time)
            .expect("Last check was in the future");

        if since_last_timer_update.as_millis() > 1000 / Machine::TIMER_FREQUENCY_IN_HERTZ {
            self.delay_timer = self.delay_timer.saturating_sub(1);
            self.sound_timer = self.delay_timer.saturating_sub(1);
            self.timers_last_update_time = now;
        }
    }

    pub fn print_memory(&self) -> () {
        println!("Memory dump of CHIP-8 virtual machine:\n{:x?}", self.memory);
    }
    
    pub fn set_memory(&mut self, address: U12, value: u8) -> () {
        debug!("Set address {:x?} to value {:x?} in memory.", address, value);
        self.memory[usize::from(address)] = value;
    }

    pub fn get_memory(&self, address: U12) -> u8 {
        debug!("Get address {:x?} with value {:x?} from memory.", address, self.memory[usize::from(address)]);
        self.memory[usize::from(address)]
    }

    pub fn copy_program_into_memory(&mut self, program: &Vec<u8>) {
        self.copy_sequence_into_memory(program, u12![Machine::PROGRAM_OFFSET_ADDRESS]);
    }
    
    pub fn copy_sequence_into_memory(&mut self, sequence: &Vec<u8>, start_address: U12) {
        for i in 0..sequence.len() {
            let offset: U12 = u12![i as u16];
            let address = start_address.checked_add(offset).expect(
                "We overflowed the memory, even though we checked before. Something is fishy.",
            );
            self.set_memory(address, sequence[i]);
        }
    }

    pub fn draw_pixel_to_buffer(&mut self, x: u8, y: u8, value: bool) {
        self.screen_buffer[x as usize][y as usize] = value;
    }

    pub fn get_pixel_value(&self, x: u8, y: u8) -> bool {
        self.screen_buffer[x as usize][y as usize]
    }

    pub fn load_font(&mut self) {
        self.copy_sequence_into_memory(&vec![0xF0, 0x90, 0x90, 0x90, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 0]); // 0
        self.copy_sequence_into_memory(&vec![0x20, 0x60, 0x20, 0x20, 0x70], u12![Machine::FONT_OFFSET_ADDRESS + 5]); // 1
        self.copy_sequence_into_memory(&vec![0xF0, 0x10, 0xF0, 0x80, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 10]); // 2
        self.copy_sequence_into_memory(&vec![0xF0, 0x10, 0xF0, 0x10, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 15]); // 3
        self.copy_sequence_into_memory(&vec![0x90, 0x90, 0xF0, 0x10, 0x10], u12![Machine::FONT_OFFSET_ADDRESS + 20]); // 4
        self.copy_sequence_into_memory(&vec![0xF0, 0x80, 0xF0, 0x10, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 25]); // 5
        self.copy_sequence_into_memory(&vec![0xF0, 0x80, 0xF0, 0x90, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 30]); // 6
        self.copy_sequence_into_memory(&vec![0xF0, 0x10, 0x20, 0x40, 0x40], u12![Machine::FONT_OFFSET_ADDRESS + 35]); // 7
        self.copy_sequence_into_memory(&vec![0xF0, 0x90, 0xF0, 0x90, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 40]); // 8
        self.copy_sequence_into_memory(&vec![0xF0, 0x90, 0xF0, 0x10, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 45]); // 9
        self.copy_sequence_into_memory(&vec![0xF0, 0x90, 0xF0, 0x90, 0x90], u12![Machine::FONT_OFFSET_ADDRESS + 50]); // A
        self.copy_sequence_into_memory(&vec![0xE0, 0x90, 0xE0, 0x90, 0xE0], u12![Machine::FONT_OFFSET_ADDRESS + 55]); // B
        self.copy_sequence_into_memory(&vec![0xF0, 0x80, 0x80, 0x80, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 60]); // C
        self.copy_sequence_into_memory(&vec![0xE0, 0x90, 0x90, 0x90, 0xE0], u12![Machine::FONT_OFFSET_ADDRESS + 65]); // D
        self.copy_sequence_into_memory(&vec![0xF0, 0x80, 0xF0, 0x80, 0xF0], u12![Machine::FONT_OFFSET_ADDRESS + 70]); // E
        self.copy_sequence_into_memory(&vec![0xF0, 0x80, 0xF0, 0x80, 0x80], u12![Machine::FONT_OFFSET_ADDRESS + 75]); // F
    }

    pub fn get_register_value(&self, register_index: usize) -> u8 {
        debug!("Get register {:x?} with value {:x?}", register_index, self.registers[register_index]);
        self.registers[register_index]
    }

    pub fn set_register_value(&mut self, register_index: usize, value: u8) {
        self.registers[register_index] = value;
        debug!("Set register {:x?} to value {:x?}", register_index, value);
    }

    pub fn get_i_register_value(&self) -> U12{
        debug!("Get i-register with value of {:x?}", self.i_register);
        self.i_register
    }

    pub fn set_i_register_value(&mut self, value: U12) {
        self.i_register = value;
        debug!("Set i-register to value {:x?}", value);
    }

    pub fn push_to_stack(&mut self, resume_address: U12) {
        self.stack.push(resume_address);
        debug!("Push resume point {:x?} to the stack", resume_address);
    }

    pub fn pop_stack(&mut self) -> U12 {
        let resume_address: U12 = self.stack.pop().unwrap();
        debug!("Pop resume point {:x?} from the stack", resume_address);
        resume_address
    }
}
